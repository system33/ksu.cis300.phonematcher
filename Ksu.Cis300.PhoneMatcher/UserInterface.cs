﻿/* UserInterface.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ksu.Cis300.PhoneMatcher
{
    public partial class UserInterface : Form
    {
        public UserInterface()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The following two linked lists store the numbers for
        /// found in each file
        /// </summary>
        LinkedListCell<PhoneCount> _file1Numbers = null;
        LinkedListCell<PhoneCount> _file2Numbers = null;

        /// <summary>
        /// Handles when file 1 button is pressed.
        /// Opens file of phone numbers and reads into
        /// linked list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxFile1Button_Click(object sender, EventArgs e)
        {
            if (uxOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    _file1Numbers = null; // reset list of numbers to be empty
                    // read file line by line
                    StreamReader sr = new StreamReader(uxOpenFileDialog.FileName);
                    string phoneNumber;
                    while ((phoneNumber = sr.ReadLine()) != null)
                    {
                        // check to see if phone number was found already
                        LinkedListCell<PhoneCount> iterator = _file1Numbers;
                        bool found = false;
                        // do so by iterating through the list
                        while (iterator != null)
                        {
                            // if number is found, update count
                            if (iterator.Data.Number == phoneNumber)
                            {
                                iterator.Data.Count++;
                                found = true;
                                break;
                            }
                            iterator = iterator.Next;
                        }
                        // if number isn't found, add it to the list
                        if (!found)
                        {
                            LinkedListCell<PhoneCount> working = new LinkedListCell<PhoneCount>();
                            working.Data = new PhoneCount(phoneNumber);
                            working.Next = _file1Numbers;
                            _file1Numbers = working;
                        }
                    }
                    // display what file is open (using base name instead of full path)
                    uxFile1Name.Text = Path.GetFileName(uxOpenFileDialog.FileName);
                    // enable "find overlaps" button if two files are open
                    uxFindOverlapsButton.Enabled = (_file1Numbers != null && _file2Numbers != null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sorry, there was an issue." + ex.ToString());
                }
            }
        }

        /// <summary>
        /// Handles when file 2 button is pressed.
        /// Opens file of phone numbers and reads into
        /// linked list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxFile2Button_Click(object sender, EventArgs e)
        {
            if (uxOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    _file2Numbers = null; // reset list of numbers to be empty
                    // read file line by line
                    StreamReader sr = new StreamReader(uxOpenFileDialog.FileName);
                    string phoneNumber;
                    while ((phoneNumber = sr.ReadLine()) != null)
                    {
                        // check to see if phone number was found already
                        LinkedListCell<PhoneCount> iterator = _file2Numbers;
                        bool found = false;
                        // do so by iterating through the list
                        while (iterator != null)
                        {
                            // if number is found, update count
                            if (iterator.Data.Number == phoneNumber)
                            {
                                iterator.Data.Count++;
                                found = true;
                                break;
                            }
                            iterator = iterator.Next;
                        }
                        // if number isn't found, add it to the list
                        if (!found)
                        {
                            LinkedListCell<PhoneCount> working = new LinkedListCell<PhoneCount>();
                            working.Data = new PhoneCount(phoneNumber);
                            working.Next = _file2Numbers;
                            _file2Numbers = working;
                        }
                    }
                    // display what file is open (using base name instead of full path)
                    uxFile2Name.Text = Path.GetFileName(uxOpenFileDialog.FileName);
                    // enable "find overlaps" button if two files are open
                    uxFindOverlapsButton.Enabled = (_file1Numbers != null && _file2Numbers != null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sorry, there was an issue." + ex.ToString());
                }
            }
        }

        /// <summary>
        /// Handles when user wants to find overlaps.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxFindOverlapsButton_Click(object sender, EventArgs e)
        {
            StringBuilder finalText = new StringBuilder(); // var for text that will go in results text box
            int minCalls = Convert.ToInt32(uxMinCallsUpDown.Value); // minimum number of calls that must be made in order to consider a number
            // iterate through list 1
            LinkedListCell<PhoneCount> iter1 = _file1Numbers;
            while (iter1 != null)
            {
                // if the number has enough calls
                if (iter1.Data.Count >= minCalls)
                {
                    // iterate through list 2 looking for same number
                    LinkedListCell<PhoneCount> iter2 = _file2Numbers;
                    while (iter2 != null)
                    {
                        // if found a matching number with enough calls
                        if (iter2.Data.Number == iter1.Data.Number && iter2.Data.Count >= minCalls)
                        {
                            // and information to the final text about this happy find
                            finalText.AppendLine(iter1.Data.Number);
                            finalText.AppendLine("          " + iter1.Data.Count + " times (file 1)");
                            finalText.AppendLine("          " + iter2.Data.Count + " times (file 2)");
                            break;
                        }
                        else iter2 = iter2.Next;
                    }
                }
                iter1 = iter1.Next;
            }
            // set text box's text to what we've calcualted or a simple info message
            if (finalText.ToString() == "")
                uxOverlapsTextBox.Text = "No overlaps found.";
            else
                uxOverlapsTextBox.Text = finalText.ToString();
        }
    }
}
