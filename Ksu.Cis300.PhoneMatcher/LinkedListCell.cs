﻿/* LinkedListCell.cs
 * Author: Rod Howell
 * namespace modified by: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksu.Cis300.PhoneMatcher
{
    /// <summary>
    /// A single cell of a linked list.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the list.</typeparam>
    public class LinkedListCell<T>
    {
        /// <summary>
        /// The data item stored in the cell.
        /// </summary>
        private T _data;

        /// <summary>
        /// Gets or sets the data item stored in the cell.
        /// </summary>
        public T Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }

        /// <summary>
        /// The next cell in the list.
        /// </summary>
        private LinkedListCell<T> _next;

        /// <summary>
        /// Gets or sets the next cell in the list.
        /// </summary>
        public LinkedListCell<T> Next
        {
            get
            {
                return _next;
            }
            set
            {
                _next = value;
            }
        }
    }
}
