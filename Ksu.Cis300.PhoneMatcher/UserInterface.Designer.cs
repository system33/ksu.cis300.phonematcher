﻿namespace Ksu.Cis300.PhoneMatcher
{
    partial class UserInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxFile1Button = new System.Windows.Forms.Button();
            this.uxFile2Button = new System.Windows.Forms.Button();
            this.uxFile1Name = new System.Windows.Forms.TextBox();
            this.uxFile2Name = new System.Windows.Forms.TextBox();
            this.uxMinCallsLabel = new System.Windows.Forms.Label();
            this.uxMinCallsUpDown = new System.Windows.Forms.NumericUpDown();
            this.uxFindOverlapsButton = new System.Windows.Forms.Button();
            this.uxOverlapsTextBox = new System.Windows.Forms.TextBox();
            this.uxOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.uxMinCallsUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // uxFile1Button
            // 
            this.uxFile1Button.Location = new System.Drawing.Point(12, 12);
            this.uxFile1Button.Name = "uxFile1Button";
            this.uxFile1Button.Size = new System.Drawing.Size(75, 23);
            this.uxFile1Button.TabIndex = 0;
            this.uxFile1Button.Text = "Load File 1";
            this.uxFile1Button.UseVisualStyleBackColor = true;
            this.uxFile1Button.Click += new System.EventHandler(this.uxFile1Button_Click);
            // 
            // uxFile2Button
            // 
            this.uxFile2Button.Location = new System.Drawing.Point(12, 41);
            this.uxFile2Button.Name = "uxFile2Button";
            this.uxFile2Button.Size = new System.Drawing.Size(75, 23);
            this.uxFile2Button.TabIndex = 1;
            this.uxFile2Button.Text = "Load File 2";
            this.uxFile2Button.UseVisualStyleBackColor = true;
            this.uxFile2Button.Click += new System.EventHandler(this.uxFile2Button_Click);
            // 
            // uxFile1Name
            // 
            this.uxFile1Name.Location = new System.Drawing.Point(93, 14);
            this.uxFile1Name.Name = "uxFile1Name";
            this.uxFile1Name.ReadOnly = true;
            this.uxFile1Name.Size = new System.Drawing.Size(132, 20);
            this.uxFile1Name.TabIndex = 2;
            this.uxFile1Name.Text = "No file loaded.";
            // 
            // uxFile2Name
            // 
            this.uxFile2Name.Location = new System.Drawing.Point(93, 43);
            this.uxFile2Name.Name = "uxFile2Name";
            this.uxFile2Name.ReadOnly = true;
            this.uxFile2Name.Size = new System.Drawing.Size(132, 20);
            this.uxFile2Name.TabIndex = 3;
            this.uxFile2Name.Text = "No file loaded.";
            // 
            // uxMinCallsLabel
            // 
            this.uxMinCallsLabel.AutoSize = true;
            this.uxMinCallsLabel.Location = new System.Drawing.Point(12, 77);
            this.uxMinCallsLabel.Name = "uxMinCallsLabel";
            this.uxMinCallsLabel.Size = new System.Drawing.Size(128, 13);
            this.uxMinCallsLabel.TabIndex = 4;
            this.uxMinCallsLabel.Text = "Minimum calls per number";
            // 
            // uxMinCallsUpDown
            // 
            this.uxMinCallsUpDown.Location = new System.Drawing.Point(146, 75);
            this.uxMinCallsUpDown.Name = "uxMinCallsUpDown";
            this.uxMinCallsUpDown.Size = new System.Drawing.Size(79, 20);
            this.uxMinCallsUpDown.TabIndex = 5;
            this.uxMinCallsUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // uxFindOverlapsButton
            // 
            this.uxFindOverlapsButton.Enabled = false;
            this.uxFindOverlapsButton.Location = new System.Drawing.Point(123, 101);
            this.uxFindOverlapsButton.Name = "uxFindOverlapsButton";
            this.uxFindOverlapsButton.Size = new System.Drawing.Size(102, 28);
            this.uxFindOverlapsButton.TabIndex = 6;
            this.uxFindOverlapsButton.Text = "Find overlaps";
            this.uxFindOverlapsButton.UseVisualStyleBackColor = true;
            this.uxFindOverlapsButton.Click += new System.EventHandler(this.uxFindOverlapsButton_Click);
            // 
            // uxOverlapsTextBox
            // 
            this.uxOverlapsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxOverlapsTextBox.Location = new System.Drawing.Point(231, 12);
            this.uxOverlapsTextBox.Multiline = true;
            this.uxOverlapsTextBox.Name = "uxOverlapsTextBox";
            this.uxOverlapsTextBox.ReadOnly = true;
            this.uxOverlapsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.uxOverlapsTextBox.Size = new System.Drawing.Size(190, 117);
            this.uxOverlapsTextBox.TabIndex = 7;
            // 
            // UserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 141);
            this.Controls.Add(this.uxOverlapsTextBox);
            this.Controls.Add(this.uxFindOverlapsButton);
            this.Controls.Add(this.uxMinCallsUpDown);
            this.Controls.Add(this.uxMinCallsLabel);
            this.Controls.Add(this.uxFile2Name);
            this.Controls.Add(this.uxFile1Name);
            this.Controls.Add(this.uxFile2Button);
            this.Controls.Add(this.uxFile1Button);
            this.Name = "UserInterface";
            this.Text = "Phone Matcher";
            ((System.ComponentModel.ISupportInitialize)(this.uxMinCallsUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button uxFile1Button;
        private System.Windows.Forms.Button uxFile2Button;
        private System.Windows.Forms.TextBox uxFile1Name;
        private System.Windows.Forms.TextBox uxFile2Name;
        private System.Windows.Forms.Label uxMinCallsLabel;
        private System.Windows.Forms.NumericUpDown uxMinCallsUpDown;
        private System.Windows.Forms.Button uxFindOverlapsButton;
        private System.Windows.Forms.TextBox uxOverlapsTextBox;
        private System.Windows.Forms.OpenFileDialog uxOpenFileDialog;
    }
}

