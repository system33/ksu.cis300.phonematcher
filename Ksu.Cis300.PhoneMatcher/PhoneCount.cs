﻿/* PhoneCount.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ksu.Cis300.PhoneMatcher
{
    class PhoneCount
    {
        /// <summary>
        /// Following two private members store actual values
        /// for a phone number and the number of times it 
        /// was called.
        /// </summary>
        private string _number;
        private int _count;

        /// <summary>
        /// Property to get/set _count
        /// </summary>
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        /// <summary>
        /// Property to get _number
        /// </summary>
        public string Number
        {
            get { return _number; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="number">string phonenumber</param>
        /// <param name="count">int number of times it was called</param>
        public PhoneCount(string number, int count = 1)
        {
            _number = number;
            _count = count;
        }
    }
}
